About
===============

* Project Name: **Fin.Ed Website v1.0**
* Client: _Finance Educators Association, Inc._ (c/o Mr Marc Gulle)
* IT Consultant / Lead Developer: _Jonathan Javellana - Smiledrop Studios Videogrammer Group (SSVG)_ 
* Project Director: _Christvinz David Lagmay - Interns Team (Fin.Ed)_
* Keywords: _personal finance, financial education, wealth, FinEd, non-profit organization, financial institution, financial literacy_
* URL: https://fin-ed.org 
* STATIC HTML: https://fin-ed.org/html
* CMS/Framework Used: Wordpress v5.8

Template used
=================

* Template Name: **Flexor - v4.3.0**
* Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
* Author: _BootstrapMade.com_
* License: https://bootstrapmade.com/license/


About Wordpress
===================

![Wordpress](https://fin-ed.org/wp-admin/images/wordpress-logo.png)

Semantic Personal Publishing Platform

## First Things First
Welcome. WordPress is a very special project to me. Every developer and contributor adds something unique to the mix, and together we create something beautiful that I&#8217;m proud to be a part of. Thousands of hours have gone into WordPress, and we&#8217;re dedicated to making it better every day. Thank you for making it part of your world.

&#8212; Matt Mullenweg

Installation: Famous 5-minute install

1. Unzip the package in an empty directory and upload everything.
2. Open [```wp-admin/install.php```](https://fin-ed.org/wp-admin/install.php) in your browser. It will take you through the process to set up a ```wp-config.php``` file with your database connection details.
	1. If for some reason this doesn&#8217;t work, don&#8217;t worry. It doesn&#8217;t work on all web hosts. Open up ```wp-config-sample.php``` with a text editor like WordPad or similar and fill in your database connection details.
	2. Save the file as ```wp-config.php``` and upload it.
	3. Open [```wp-admin/install.php```](http://fin-ed.org/wp-admin/install.php) in your browser.
3. Once the configuration file is set up, the installer will set up the tables needed for your site. If there is an error, double check your ```wp-config.php``` file, and try again. If it fails again, please go to the [WordPress support forums](https://wordpress.org/support/forums/) with as much data as you can gather.
4. If you did not enter a password, note the password given to you. If you did not provide a username, it will be ```admin```.
5. The installer should then send you to the [login page](https://fin-ed.org/login). Sign in with the username and password you chose during the installation. If a password was generated for you, you can then click on &#8220;Profile&#8221; to change the password.

## Updating

### Using the Automatic Updater
1. Open [wp-admin/update-core.php](https://fin-ed.org/wp-admin/update-core.php) in your browser and follow the instructions.
2. You wanted more, perhaps? That&#8217;s it!


### Updating Manually

1. Before you update anything, make sure you have backup copies of any files you may have modified such as ```index.php```.
2. Delete your old WordPress files, saving ones you&#8217;ve modified.
3. Upload the new files.
4. Point your browser to [/wp-admin/upgrade.php](wp-admin/upgrade.php).

## Online Resources

If you have any questions that aren&#8217;t addressed in this document, please take advantage of WordPress&#8217; numerous online resources:

[The WordPress Codex](https://codex.wordpress.org/)
	The Codex is the encyclopedia of all things WordPress. It is the most comprehensive source of information for WordPress available.
[The WordPress Blog](https://wordpress.org/news/)
	This is where you&#8217;ll find the latest updates and news related to WordPress. Recent WordPress news appears in your administrative dashboard by default.
[WordPress Planet](https://planet.wordpress.org/)
	The WordPress Planet is a news aggregator that brings together posts from WordPress blogs around the web.
[WordPress Support Forums](https://wordpress.org/support/forums/)
	If you&#8217;ve looked everywhere and still can&#8217;t find an answer, the support forums are very active and have a large community ready to help. To help them help you be sure to use a descriptive thread title and describe your question in as much detail as possible.
[WordPress **IRC**(Internet Relay Chat) Channel](https://make.wordpress.org/support/handbook/appendix/other-support-locations/introduction-to-irc/)
	There is an online chat channel that is used for discussion among people who use WordPress and occasionally support topics. The above wiki page should point you in the right direction. ([irc.libera.chat](https://web.libera.chat/#wordpress) #wordpress)

## Final Notes

* If you have any suggestions, ideas, or comments, or if you (gasp!) found a bug, join us in the [Support Forums](https://wordpress.org/support/forums/).
* WordPress has a robust plugin **API** (Application Programming Interface) that makes extending the code easy. If you are a developer interested in utilizing this, see the [Plugin Developer Handbook](https://developer.wordpress.org/plugins/). You shouldn&#8217;t modify any of the core code.


## Share the Love

WordPress has no multi-million dollar marketing campaign or celebrity sponsors, but we do have something even better&#8212;you. If you enjoy WordPress please consider telling a friend, setting it up for someone less knowledgeable than yourself, or writing the author of a media article that overlooks us.

WordPress is the official continuation of [b2/caf&#233;log](http://cafelog.com), which came from Michel V. The work has been continued by the [WordPress developers](https://wordpress.org/about/). If you would like to support WordPress, please consider [donating](https://wordpress.org/donate).

## License

WordPress is free software, and is released under the terms of the __GPL (GNU General Public License)__ version 2 or (at your option) any later version. See [license.txt](https://fin-ed.org/license.txt).