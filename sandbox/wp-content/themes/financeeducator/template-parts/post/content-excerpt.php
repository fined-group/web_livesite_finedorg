<?php
/**
 * Template part for displaying posts with excerpts
 *
 * Used in Search Results and for Recent Posts in Front Page panels.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.2
 */

?>

<article id="post-<?php the_ID(); ?>" class="entry entry-single">
	<div class="entry-img">
		<?php the_post_thumbnail( 'full' ); ?>
	</div>
	<header class="entry-header">
		<?php
		if ( is_front_page() && ! is_home() ) {

			// The excerpt is being displayed within a front page section, so it's a lower hierarchy than h2.
			the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' );
		} else {
			the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
		}
		?>
	</header><!-- .entry-header -->
	<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<ul>
				<li class="d-flex align-items-center">
					<i class="bi bi-clock"></i> 
					<?=financeeducator_time_link();?>
				</li>
				<li class="d-flex align-items-center">
					<?php
						financeeducator_edit_link();
					?>
				</li>
			</ul>
		</div><!-- .entry-meta -->
	<?php elseif ( 'page' === get_post_type() && get_edit_post_link() ) : ?>
		<div class="entry-meta">
			<?php financeeducator_edit_link(); ?>
		</div><!-- .entry-meta -->
	<?php endif; ?>

	<div class="entry-summary entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

</article><!-- #post-<?php the_ID(); ?> -->
