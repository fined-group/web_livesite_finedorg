<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

get_header(); ?>

<main id="main">

	<!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="/">Home</a></li>
          <li><a href="blog">Blog</a></li>
        </ol>
		<?php the_title( '<h2>', '</h2>' ); ?>
      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Blog Single Section ======= -->
    <section id="blog" class="blog">
      <div class="container" data-aos="fade-up">

        <div class="row">

			<div class="col-lg-8 entries">

			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/post/content', get_post_format() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

				the_post_navigation(
					array(
						'prev_text' => '<span class="screen-reader-text">' . __( 'Previous Post', 'financeeducator' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Previous', 'financeeducator' ) . '</span> <span class="nav-title"><span class="nav-title-icon-wrapper">' . financeeducator_get_svg( array( 'icon' => 'arrow-left' ) ) . '</span>%title</span>',
						'next_text' => '<span class="screen-reader-text">' . __( 'Next Post', 'financeeducator' ) . '</span><span aria-hidden="true" class="nav-subtitle">' . __( 'Next', 'financeeducator' ) . '</span> <span class="nav-title">%title<span class="nav-title-icon-wrapper">' . financeeducator_get_svg( array( 'icon' => 'arrow-right' ) ) . '</span></span>',
					)
				);

			endwhile; // End the loop.
			?>

			</div>
			<div class="col-lg-4">
				<?php get_sidebar(); ?>
			</div><!-- End blog sidebar -->

      </div>
    </section><!-- End Blog Single Section -->

</main><!-- End #main -->

<?php
get_footer();
