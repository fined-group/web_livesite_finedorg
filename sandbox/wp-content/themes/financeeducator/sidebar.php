<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>
<div class="sidebar" aria-label="<?php esc_attr_e( 'Blog Sidebar', 'financeeducator' ); ?>">
	<?php dynamic_sidebar( 'sidebar-1' ); ?>
</div>

<!-- <aside id="secondary" class="widget-area" role="complementary" aria-label="<?php esc_attr_e( 'Blog Sidebar', 'financeeducator' ); ?>">
</aside> -->
<!-- #secondary -->
