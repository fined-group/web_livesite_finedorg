<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.2
 */

?>

<!-- ======= Footer ======= -->
<footer id="footer">

  <div class="footer-top">
    <div class="container">
      <div class="row">

        <div class="col-lg-3 col-md-6 footer-contact">
          <h3 class="text-white">Fin.Ed</h3>
          <p>
            1605 Cityland 10 Towers 1,<br> 
            H.V. Dela Costa St, Salcedo Village <br>
            Makati City, Metro Manila<br>
            Philippines<br><br>
            <strong>Phone:</strong> +63 933 866 9979<br>
            <strong>Email:</strong> secretariat@fin-ed.org<br>
          </p>
        </div>

        <div class="col-lg-2 col-md-6 footer-links">
          <h4>Useful Links</h4>
          <ul>
            <li><i class="bx bx-chevron-right"></i> <a href="/">Home</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
            <li><i class="bx bx-chevron-right"></i> <a href="/privacy-policy/">Privacy policy</a></li>
          </ul>
        </div>

        <div class="col-lg-3 col-md-6 footer-links"></div>

        <div class="col-lg-4 col-md-6 footer-newsletter">
          <h4>Like us on Facebook!</h4>
          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Ffin.educators&tabs=timeline&width=500&height=500&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="500" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
        </div>

      </div>
    </div>
  </div>

  <div class="container d-lg-flex py-4">

    <div class="me-lg-auto text-center text-lg-start">
      <div class="copyright">
        &copy; Copyright 2021 <strong><span>Finance Educators Association (Fin.Ed)</span></strong>. All Rights Reserved
      </div>
      <div class="credits text-white">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/ -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>, <a href="https://ssvideogrammer.com/">SSVG Partners &copy; 2021</a>
      </div>
    </div>
    <div class="social-links text-center text-lg-right pt-3 pt-lg-0">
      <a href="https://facebook.com/fin.educators" class="facebook"><i class="bx bxl-facebook"></i></a>
      <a href="#" class="youtube"><i class="bx bxl-youtube"></i></a>
    </div>
  </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Objectives Modal -->
<div class="modal fade" id="ourObjectivesMod" data-bs-backdrop="static" data-bs-keyboard="true" tabindex="-1" aria-labelledby="ourObjectivesModLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-fullscreen-md-down">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ourObjectivesModLabel">About Fin.Ed</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <article>
          <p class="reset mb-4 lh-lg">
            The <b><i>Finance Educators Association of the Philippines</i></b> (Fin.Ed) was formed in April 2012 as a nationwide organization that seeks to upgrade the level of financial education in the country by promoting the professional development and welfare of finance educators.  Fin.Ed also aims to be a catalyst for widespread financial literacy that contributes to economic upliftment.  Fin.Ed is a registered company under the Philippines Securities & Exchange Commission (SEC) and is recognized by the Commission on Higher Education (CHED). Through its student & professional organization affiliates, the Junior Confederation of Finance Associations – Philippines (JCFAP) and the Chamber of Finance Associations & Professionals (CFAP), Fin.Ed reaches over 200 schools with 10 chapters throughout the country as of February 2021.
          </p>
          <details>
            <summary class="fw-bold fs-4">Objectives</summary>
            <ol>
              <li>To serve as the Professional Organization in the field of Finance;</li>
              <li>As a nationwide organization that seeks to upgrade the level of financial education in the country by promoting the professional development and welfare of the finance educators;</li>
              <li>To be the catalyst for widespread financial literacy that contributes to economic and social development;</li>
              <li>To effectively and efficiently respond to the challenges of financial education globally;</li>
              <li>To harmonize linkages both in the Industries, Academe, and Financial institutions either public or private entities.</li>
            </ol>
          </details>
        </article>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Vendor JS Files -->
<script src="<?=get_template_directory_uri();?>/assets/vendor/aos/aos.js"></script>
<script src="<?=get_template_directory_uri();?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?=get_template_directory_uri();?>/assets/vendor/glightbox/js/glightbox.min.js"></script>
<script src="<?=get_template_directory_uri();?>/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?=get_template_directory_uri();?>/assets/vendor/php-email-form/validate.js"></script>
<script src="<?=get_template_directory_uri();?>/assets/vendor/swiper/swiper-bundle.min.js"></script>

<!-- Template Main JS File -->
<script src="<?=get_template_directory_uri();?>/assets/js/main.js"></script>
<script src="https://cdn.jsdelivr.net/gh/jquery/jquery@3.2.1/dist/jquery.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>
<script src="<?=get_template_directory_uri();?>/assets/js/home/index.js"></script>

<!-- Custom JS Files -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- WP Footer -->
<?php wp_footer(); ?>

</body>

</html>