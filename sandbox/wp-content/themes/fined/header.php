<?php
/**
 * The header.
 *
 * This is the template that displays all of the <head> section and everything up until main.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package SSVideogrammer
 * @subpackage FinEd
 * @since FinEd 1.0
 */


?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> style="margin-top:0">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Home - This is Fin.Ed</title>
  <meta content="The Official Website of Finance Educators Association - FinEd in the Philippines. Let's make Financial Literacy accessible to every Juan." name="description">
  <meta content="Personal Finance, Financial Education, Wealth, Organization, Non-profit" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" rel="icon">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css" type="text/css" media="all" />
  <!--homepage css-->
  <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/css/homepage/index.css">
  <link rel="stylesheet" href="<?=get_template_directory_uri();?>/assets/css/homepage/index2.css">
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-bootstrap-4/bootstrap-4.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
  

  <!-- =======================================================
  * Project Name: Fin.Ed Web v1
  * Template Name: Flexor - v4.3.0
  * Template URL: https://bootstrapmade.com/flexor-free-multipurpose-bootstrap-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  * Client: Marc Gulle - Finance Educators Group (FinEd)
  * IT Consultant: Jonathan Javellana - Smiledrop Studios Videogrammer (SSVG) 
  * Project Director: Christvinz David Lagmay - Interns Team (FinEd)
  ======================================================== -->
  
  <?php wp_head(); ?>
</head>

<body>
<?php wp_body_open(); ?>
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fe-bg-dblue">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center fe-color-creamwhite"><a href="mailto:secretariat@fin-ed.org">secretariat@fin-ed.org</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4 fe-color-creamwhite"><span>+63 933 866 9979</span></i>
      </div>

      <div class="cta d-none d-md-flex align-items-center fe-bg-lightblue">
        <a href="#login" class="scrollto"><i class="bi bi-arrow-up-right-circle-fill"></i>&nbsp; Login</a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <a href="/"><img src="<?=get_template_directory_uri();?>/assets/img/logo.png" alt="" class="img-fluid"></a>
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Advocacies</a></li>
          <li><a class="nav-link scrollto" href="#programs">Programs</a></li>
          <li><a class="nav-link scrollto " href="#portfolio">Gallery</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li><a href="#">Blog</a></li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->
