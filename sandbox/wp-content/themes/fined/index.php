<?php 
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package SSVideogrammer
 * @subpackage FinEd
 * @since FinEd 1.0
 */

get_header(); ?>

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex flex-column justify-content-center align-items-center">
    <div class="container" data-aos="fade-in">
      <h1>Welcome to Fin.Ed</h1>
      <h2>The nationwide organization for finance educators & practitioners</h2>
      <div class="d-flex align-items-center">
        <i class="bx bxs-right-arrow-alt get-started-icon"></i>
        <a href="https://tinyurl.com/financeeducatorsmembership1" class="btn-get-started scrollto">Be a member now</a>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
      <div class="container">

        <div class="row">
          <div class="col-xl-4 col-lg-5" data-aos="fade-up">
            <div class="content">
              <h3>Why become a Fin.Ed member?</h3>
              <p>
                Joining us will give you access to the following perks: networking between members, partnering with relevant organizations, FLP seminars, financial articles, publications, and journals and continuing professional development!
              </p>
              <div class="text-center">
                <a href="#" class="more-btn" data-bs-toggle="modal" data-bs-target="#ourObjectivesMod">Learn More <i class="bx bx-chevron-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-xl-8 col-lg-7 d-flex">
            <div class="icon-boxes d-flex flex-column justify-content-center">
              <div class="row">
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-bulb"></i>
                    <h4>TEACH</h4>
                    <p>We are a family of finance educators and professionals, recognizing excellent performance in the field of business education, especially in finance</p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-line-chart"></i>
                    <h4>BUSINESS</h4>
                    <p>By sharing past and current best practices, and disseminating the latest developments & innovation in different industries, we strive to help one another in our respective endeavors
                    </p>
                  </div>
                </div>
                <div class="col-xl-4 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                  <div class="icon-box mt-4 mt-xl-0">
                    <i class="bx bx-check-circle"></i>
                    <h4>BETTER</h4>
                    <p>The advancement of financial literacy and inclusion of every Filipino is our main objective, as we keep on expanding and growing as a non-profit organization</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= About Section ======= -->
    <section id="about" class="about section-bg">
      <div class="container">

        <div class="row">
          <div class="col-xl-5 col-lg-6 video-box d-flex justify-content-center align-items-stretch position-relative" data-aos="fade-right">
            <!-- <a href="https://youtu.be/s1cbyay0MWs" class="glightbox play-btn mb-4"></a> -->
          </div>

          <div class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-start py-5 px-lg-5">
            <h4 data-aos="fade-up">About us</h4>
            <h3 data-aos="fade-up">Fin.Ed is the official organization for finance educators and practitioners in Asia.</h3>
            <p data-aos="fade-up" class="pt-5">The Finance Educators Association of the Philippines (Fin.Ed) was formed in April 2012 as a nationwide organization that seeks to upgrade the level of financial education in the country by promoting the professional development and welfare of finance educators. Fin.Ed also aims to be a catalyst for widespread financial literacy that contributes to economic upliftment.</p>
            <div class="d-flex flex-row justify-content-start pt-4">
              <button type="button" data-aos="fade-up" class="btn btn-outline-primary btn-ourObjectives btn-lg" data-bs-toggle="modal" data-bs-target="#ourObjectivesMod">OUR OBJECTIVES <i class="bx bx-chevron-right"></i></button>
            </div>
            <!-- <div class="icon-box" data-aos="fade-up">
              <div class="icon"><i class="bx bx-fingerprint"></i></div>
              <h4 class="title"><a href="">Lorem Ipsum</a></h4>
              <p class="description">Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident</p>
            </div> -->

            <!-- <div class="icon-box" data-aos="fade-up" data-aos-delay="100">
              <div class="icon"><i class="bx bx-gift"></i></div>
              <h4 class="title"><a href="">Nemo Enim</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>

            <div class="icon-box" data-aos="fade-up" data-aos-delay="200">
              <div class="icon"><i class="bx bx-atom"></i></div>
              <h4 class="title"><a href="">Dine Pad</a></h4>
              <p class="description">Explicabo est voluptatum asperiores consequatur magnam. Et veritatis odit. Sunt aut deserunt minus aut eligendi omnis</p>
            </div> -->

          </div>
        </div>

      </div>
    </section><!-- End About Section -->

    <!-- ======= Partners Section ======= -->
    <section id="clients" class="clients">
      <div class="container" data-aos="fade-up">

        <div 
          class="section-title text-center" 
          data-aos="fade-down"
        >
          <img src="<?=get_template_directory_uri();?>/assets/img/partners/title-flp.png" alt="logo of financial literacy partners" class="img-fluid" style="max-width: 68%;">
        </div>

        <div class="clients-slider swiper-container" data-aos="fade-down">
          <div class="swiper-wrapper align-items-center">
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-1.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-2.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-3.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-4.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-5.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-6.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-7.png" class="img-fluid" alt=""></div>
            <div class="swiper-slide"><img src="<?=get_template_directory_uri();?>/assets/img/partners/partner-8.png" class="img-fluid" alt=""></div>
          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Partners Section -->

    <!-- ======= Our Advocacies ======= -->
    <section id="services" class="services section-bg advocacy-bg">
      <div class="container">

        <div class="section-title " data-aos="fade-up">
          <h2 class="text-white py-4">Our Advocacies</h2>
        </div>

        <div class="row">
          <div class="col-lg-4 col-md-6" data-aos="fade-up">
            <div class="icon-box h-75">
              <div class="icon"><i class="bi bi-briefcase"></i></div>
              <p class="description my-4 mx-4">
                Upgrade the level of Financial Education in the Philippines by promoting the professional development and welfare of Finance Educators.
              </p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box h-75">
              <div class="icon"><i class="bi bi-bar-chart"></i></div>
              <p class="description my-4 mx-4">
                Propagate and develop financial literacy programs in schools as well as private sectors, as this will help equip our people with appropriate knowledge on how to properly manage their finances and make well-informed financial decisions.
              </p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box h-75">
              <div class="icon"><i class="bi bi-card-checklist"></i></div>
              <p class="description my-4 mx-4">Fact-checking the veracity of Finance programs, institutes, or organizations that promote financial literacy, with an aim of consumer protection from financial scams, fraud, and other finance-related crimes and unusual schemes. </p>
            </div>
          </div>
          <!-- <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bi bi-binoculars"></i></div>
              <h4 class="title"><a href="">Magni Dolores</a></h4>
              <p class="description">Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bi bi-brightness-high"></i></div>
              <h4 class="title"><a href="">Nemo Enim</a></h4>
              <p class="description">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
            </div>
          </div>
          <div class="col-lg-4 col-md-6" data-aos="fade-up" data-aos-delay="500">
            <div class="icon-box">
              <div class="icon"><i class="bi bi-calendar4-week"></i></div>
              <h4 class="title"><a href="">Eiusmod Tempor</a></h4>
              <p class="description">Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi</p>
            </div>
          </div> -->
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Values Section ======= -->
    <section id="values" class="values">
      <div class="container">

        <div class="row">
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card" style="background-image: url(<?=get_template_directory_uri();?>/assets/img/values-1.jpg);">
              <div class="card-body">
                <h5 class="card-title">Our Mission</h5>
                <p class="card-text">Fin.Ed shall build strong leaders and be the catalyst for change and innovation in finance education, and foster fidelity to high ethical standards and a commitment to excellence.</p>
                <!-- <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Read More</a></div> -->
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="fade-up" data-aos-delay="100">
            <div class="card" style="background-image: url(<?=get_template_directory_uri();?>/assets/img/values-2.jpg);">
              <div class="card-body">
                <h5 class="card-title">Our Vision</h5>
                <p class="card-text">Fin.Ed is a community of professionals recognized as the leading resource for finance education and financial literacy in the country and region.</p>
                <!-- <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Read More</a></div> -->
              </div>
            </div>

          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="card" style="background-image: url(<?=get_template_directory_uri();?>/assets/img/values-3.jpg);">
              <div class="card-body">
                <h5 class="card-title">Our Story</h5>
                <p class="card-text">Finance Educators Association is the mother organization of JCFAP, founded by two finance professionals, Antonilo Mauricio and Alexander Gilles, who realized a need for an organization among finance education professionals.</p>
                <!-- <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Read More</a></div> -->
              </div>
            </div>
          </div>
          <div class="col-md-6 d-flex align-items-stretch mt-4" data-aos="fade-up" data-aos-delay="300">
            <div class="card" style="background-image: url(<?=get_template_directory_uri();?>/assets/img/values-4.jpg);">
              <div class="card-body">
                <h5 class="card-title">Our Affiliates</h5>
                <p class="card-text">Fin.Ed reaches over 200 schools with 10 chapters throughout the country as of February 2021.</p>
                <!-- <div class="read-more"><a href="#"><i class="bi bi-arrow-right"></i> Read More</a></div> -->
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Values Section -->

    <!-- ======= Testimonials Section ======= -->
    <section id="testimonials" class="testimonials">
      <div class="container position-relative" data-aos="fade-up">

        <div class="testimonials-slider swiper-container" data-aos="fade-up" data-aos-delay="100">
          <div class="swiper-wrapper">

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-1.jpg" class="testimonial-img" alt="Sir Antonilo Mauricio's profile picture for testimonial">
                <h3>Anton Mauricio</h3>
                <h4>Founder &amp; Chairman, Finance Educators Association</h4>
                <p class="fs-3" >
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fin.Ed has enabled me to get to know the concerns of educators from all over the country. It is inspiring to realize that there are so many good-hearted teachers who care so much about their students.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-2.jpg" class="testimonial-img" alt="Sir Alexander Gilles' profile picture for testimonial">
                <h3>Alexander Gilles</h3>
                <h4>Co Chairman, Finance Educators Association</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fin.Ed is the opportunity for finance professors to pursue continuing education. This manner of "sharpening the saw" should lead to enhancements in their careers. Professors may transition to industry, then move back to teaching, with greater insights.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-3.jpg" class="testimonial-img" alt="Sir Melvin Jason de Vera's profile picture for testimonial">
                <h3>Melvin Jason de Vera</h3>
                <h4>President, Finance Educators Association</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fin.Ed bridges the gap between Finance Education and the competency needs in the Finance Industry by strengthening linkages between educators and industry practitioners in Finance. It has helped finance educators to enhance their networks, hone skills, improve pedagogy and be more contributory in raising the quality of financial education in the country.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-4.jpg" class="testimonial-img" alt="Ms Marycris Albao's profile picture for testimonial">
                <h3>Marycris Albao</h3>
                <h4>Member, Financial Management Department, De La Salle University - Manila</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  The Finance Educators Association provided seminars and linkages to keep the members updated with the needs and trends of teaching finance courses. It also provides the opportunity to be acquainted with other professors in various colleges and universities in the country. Hence, it is very rewarding to be a member of the Finance Educators Association.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-5.jpg" class="testimonial-img" alt="Reuben Maynard N. Quero profile picture for testimonial">
                <h3>Prof. Meynard Ern Quero</h3>
                <h4>Member, National University</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Being a member of Fin.Ed equips an educator with confidence to impart students real-life knowledge and information based on real-life skills and real-life experience. As an active trader, investor, and entrepreneur of two ventures (fitness center and a soon-to-launch and ongoing registration Information Technology based solution), Fin.Ed fortifies my financial wisdom and strengthens my will to face and take calculated risk, with reason and objective to make informed decisions.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-6.jpg" class="testimonial-img" alt="Reuben Maynard N. Quero profile picture for testimonial">
                <h3>Prof. Bernadette Panibio</h3>
                <h4>Member, Polytechnic University of the Philippines</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fin.Ed provided me with professional development and networking opportunities through the seminars/conferences it conducted. During these events, skills and knowledge were acquired and enhanced. Also, being around other finance educators and practitioners exposed me to new ideas and outlooks.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

            <div class="swiper-slide">
              <div class="testimonial-item">
                <img src="<?=get_template_directory_uri();?>/assets/img/testimonials/testimonials-7.jpg" class="testimonial-img" alt="Reuben Maynard N. Quero profile picture for testimonial">
                <h3>Asst. Prof. Bernard R. Letrero</h3>
                <h4>Member, IE, MBA, CFMP, Assistant Dean, PLM Business School</h4>
                <p class="fs-3">
                  <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                  Fin.Ed helps widen my exposure to industry trends by experiencing hands-on coaching, training, and company visits. These prepare me to address the challenges of guiding our students towards their personal goals in completing their courses.
                  <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                </p>
              </div>
            </div><!-- End testimonial item -->

          </div>
          <div class="swiper-pagination"></div>
        </div>

      </div>
    </section><!-- End Testimonials Section -->

   <!--portfolio/gallery section here-->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team bg-secondary">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up" class="text-light">Team</h2>
          <p data-aos="fade-up" class="text-light">The Current Board &amp; Management of Finance Educators Association are expected to perform their duties and responsibilities in the organization, as they aim to connect with finance educators and practitioners and build a stronger partnership and alliance towards excellence.</p>
        </div>

        <div class="row mt-5">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-1.jpg" class="img-fluid" alt="Anton Mauricio Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/anton-mauricio-5239711a/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Antonilo Mauricio</h4>
                <span>Founder and Chairman</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-2.jpg" class="img-fluid" alt="Alexander Gilles Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/alexander-n-sandy-gilles-6b03bb4/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Alexander Gilles, CFA</h4>
                <span>Co-Chairman</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-3.jpg" class="img-fluid" alt="Arman Cruz Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/arman-cruz-38060b41/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Arman Vicencio Cruz</h4>
                <span>Founding President</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-4.jpg" class="img-fluid" alt="Melvin De Vera Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/melvin-jason-de-vera-cis-cfmp-afa-6a8055116/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Melvin Jason De Vera, CIS</h4>
                <span>President</span>
              </div>
            </div>
          </div>

        </div>

        
        <div class="row justify-content-center mt-5">

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-5.jpg" class="img-fluid" alt="Marc Gulle Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/marckristiangulle/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Marc Kristian Gulle</h4>
                <span>Secretary</span>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
            <div class="member">
              <div class="member-img">
                <img src="<?=get_template_directory_uri();?>/assets/img/team/team-6.jpg" class="img-fluid" alt="Rafaelito Solangon Portrait">
                <div class="social">
                  <a href="https://www.linkedin.com/in/rafaelito-solangon-16093028/"><i class="bi bi-linkedin"></i></a>
                </div>
              </div>
              <div class="member-info">
                <h4>Rafaelito Solangon, CPA</h4>
                <span>Treasurer</span>
              </div>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Team Section -->

    <!-- ======= F.A.Q Section ======= -->
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">FAQ</h2>
        </div>

        <div class="faq-list">
          <ul>
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" class="collapse" data-bs-target="#faq-list-1">What are the qualifications? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-1" class="collapse show" data-bs-parent=".faq-list">
                
                  <ol>
                    <li>Finance Teachers, Lecturers, University Administrators</li>
                    <li>Financial Literacy Advocates, Academics, and Practitioners</li>
                    <li>Finance Graduates - Bachelor in Financial Management or BSBA major in Finance</li>
                    <li>Financial Service Professionals who want to Promote Financial  Literacy as their Advocacy</li>
                    <li>JCFAP Alumni</li>
                    <li>Finance/Business Graduate Students</li>
                  </ol>
                  
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-2" class="collapsed">What can I expect once I become a member? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-2" class="collapse" data-bs-parent=".faq-list">
                <div class="mt-4">
                  <p class="h4">Benefits:</p>
                  <div class="px-5">
                    <p>Membership Card</p>
                    <p>Membership Certificate</p>
                    <p>Membership Badge</p>
                    <p>Membership Verification System via QR Code</p>
                    <p>Special access/discounts for Fin.Ed events</p>
                  </div>
                  <p class="h4">Special access/discounts for membership and events conducted by the Financial Literacy Partners (FLP)</p>
                    <div class="px-5">
                      <p>Junior Confederation of Finance Associations - Philippines (JCFAP)</p>
                      <p>JCFAP Alumni Association (JAA)</p>
                      <p>Chamber of Finance Associations and Professionals (CFAP)</p>
                      <p>Finance Education Talks (FEdTalks)</p>
                      <p>Finance Education Center (FEdCenter)</p>
                      <article>
                        <p>Wealth Educators</p>
                        <div class="px-3">
                          <p>Support for career placement</p>
                          <p>Access to customized trainings, workshops, and certification programs</p>
                          <p>Registry for internships, grants, endowments, and scholarships (if available)</p>
                          <p>Speakers bureau</p>
                          <p>Network and linkages</p>
                        </div>
                      </article>
                    </div>
                </div>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-3" class="collapsed">What does Fin.Ed represent or stands for?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-3" class="collapse" data-bs-parent=".faq-list">
                <p>
                  Fin.Ed is a <abbr title="a blending of words/phrases which forms a new meaning">portmanteau</abbr> of the words, <q><i>Finance</i></q> and <q><i>Educator</i></q>. An association that represents finance teachers, lecturers, administrators, business schools with finance courses, financial literacy advocates, academic regulators, and partner stakeholders. We promote financial literacy and we make financial education accessible to the every Filipino.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-4" class="collapsed">What are the requirements to join? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-4" class="collapse" data-bs-parent=".faq-list">
                <p>
                  <ol>
                    <li>
                      Paid Membership Fee (with proof of payment) - File name: Surname_FirstName_Payment
                    </li>
                    <li>
                      Updated CV - File name: Surname_FirstName_CV
                    </li>
                    <li>
                      2x2 ID Picture (formal, white background) - File name: Surname_FirstName_ID
                    </li>
                    <li>
                      Accomplished Membership Form
                    </li>
                  </ol>
                </p>
                <p>*Complete the membership form and upload all the requirements via this link: <a href="https://tinyurl.com/financeeducatorsmembership1">https://tinyurl.com/financeeducatorsmembership1</a></p>
                <p>*If you can't access the link above, please use this link: <a href="https://tinyurl.com/financeeducatorsmembership2">https://tinyurl.com/financeeducatorsmembership2</a></p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-bs-toggle="collapse" data-bs-target="#faq-list-5" class="collapsed">How can I pay my membership fee?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="faq-list-5" class="collapse" data-bs-parent=".faq-list">
                <div class="list-unstyled mt-4">
                  <p>You can pay via Bank Deposit or Online Transfer.</p>
                  <p class="mb-4">Deposit or transfer the appropriate membership fee (Php 1,000.00 for new application, Php 500.00 for renewal / JCFAP Alumni) to the bank account below.</p>

                  <p>BDO Account Name: <strong>Junior Confederation of Finance Associations - Philippines</strong><br>
                  Account No: <strong>007280014787</strong></p>

                  <p class="mb-4">Save a scanned copy of your deposit slip or screenshot of your online transfer. The proof of payment must show the total amount paid, the account name, and the account number.</p>

                  <p class="mb-4">Accomplish the Membership Form and upload your proof of payment at the end of the registration form.</p>

                  <p class="mb-4 d-flex">If you have any questions, send us an email at : <span><a href="mailto:secretariat@fin-ed.org">secretariat@fin-ed.org</a></span></p>
                </div>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section><!-- End F.A.Q Section -->
    <!--CALL TO ACTION-->
    <div class="callbox">
      <div class="cta-banner text-center">
      <h2 class="title">We look forward to advancing the standards of financial education with all of you</h2>
      <p>Build connections with the best finance educators and practitioners in our industry! Together, let us teach business better with Fin.Ed!</p>
      <a class="btn learnmorebtn" href="https://tinyurl.com/financeeducatorsmembership2">Be a member now &nbsp; <i class="fas fa-long-arrow-alt-right"></i></a></div>
    </div>

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

        <div class="section-title">
          <h2 data-aos="fade-up">Contact</h2>
          <p data-aos="fade-up">With our organization by your side, there’s no telling where your next small steps could lead. You can reach us through our following social media accounts and contacts:</p>
        </div>

        <!-- MAP -->
        <div id="row justify-content-center">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d663.2207449486349!2d121.01670916302521!3d14.560428877715967!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c9096ec55555%3A0xaf0d621b7e9c77c1!2sCityland%2010%20Tower%201!5e0!3m2!1sen!2sph!4v1633158166167!5m2!1sen!2sph" width="100%" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>

        <div class="row justify-content-center">

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up">
            <div class="info-box h-100">
              <i class="bx bx-map"></i>
              <h3>Our Address</h3>
              <p>
                Unit 603 Cityland 10, Tower 1, H.V. Dela Costa St., Salcedo Village, Makati City, PH
              </p>
            </div>
          </div>

          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="100">
            <div class="info-box h-100">
              <i class="bx bx-envelope"></i>
              <h3>Email Us</h3>
              <p><a href="mailto:secretariat@fin-ed.org">secretariat@fin-ed.org</a></p>
            </div>
          </div>
          <div class="col-xl-3 col-lg-4 mt-4" data-aos="fade-up" data-aos-delay="200">
            <div class="info-box h-100">
              <i class="bx bx-phone-call"></i>
              <h3>Call Us</h3>
              <p><a href="tel:+639338669979">+63 933 866 9979</a><br>Messenger: <a href="https://m.me/fin.educators">m.me/fin.educators</a></p>
            </div>
          </div>
        </div>

        <div class="row justify-content-center" data-aos="fade-up" data-aos-delay="300">
          <div class="col-xl-9 col-lg-12 mt-4">
            <form action="api/v1/forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Message" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->
    
    <section class="inner-page pt-3">
      <div class="container">
      <?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>
	  <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	    <div class="post-header">
	       <div class="date"><?php the_time( 'M j y' ); ?></div>
	       <h2><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	       <div class="author"><?php the_author(); ?></div>
	    </div><!--end post header-->
	    <div class="entry clear">
	       <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail(); ?>
	       <?php the_content(); ?>
	       <?php edit_post_link(); ?>
	       <?php wp_link_pages(); ?> </div>
	    <!--end entry-->
	    <div class="post-footer">
	       <div class="comments"><?php comments_popup_link( 'Leave a Comment', '1 Comment', '% Comments' ); ?></div>
	    </div><!--end post footer-->
	    </div><!--end post-->
	<?php endwhile; /* rewind or continue if all posts have been fetched */ ?>
	    <div class="navigation index">
	       <div class="alignleft"><?php next_posts_link( 'Older Entries' ); ?></div>
	       <div class="alignright"><?php previous_posts_link( 'Newer Entries' ); ?></div>
	    </div><!--end navigation-->
	<?php else : ?>
	<?php endif; ?>

      </div>
    </section>


  </main><!-- End #main -->
  
  <?php get_sidebar(); ?>
  <?php get_footer(); ?>
