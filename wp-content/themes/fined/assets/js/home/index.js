/**
* Homepage Index 
* 
* Author: Ssvideogrammer.com / Jonathan Javellana
* 
*/
var options = {
    keyboard: true,
    focus: true
}
var myModal = new bootstrap.Modal(document.getElementById('welcomeMessageMod'), options)

if(sessionStorage.getItem('modalShown') != 'yes'){
    setTimeout(() => {
        myModal.show();
    }, 3000);
    sessionStorage.setItem('modalShown', 'yes');
}
