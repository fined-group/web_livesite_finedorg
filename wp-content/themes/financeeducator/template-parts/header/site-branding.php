<?php
/**
 * Displays header site branding
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

?>
<?php the_custom_logo(); ?>
