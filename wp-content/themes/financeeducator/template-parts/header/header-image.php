<?php
/**
 * Displays header media
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

?>
<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>