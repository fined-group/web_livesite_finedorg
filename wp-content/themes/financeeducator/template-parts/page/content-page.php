<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

?>
 <!-- ======= Breadcrumbs ======= -->
 <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">
        <ol>
          <li><a href="<?=site_url();?>">Home</a></li>
          <li><?php the_title();?></li>
        </ol>
		<?php the_title('<h2>', '</h2>'); ?>
      </div>
</section><!-- End Breadcrumbs -->

<section class="inner-page pt-3">
      <div class="container">
		  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			  <header class="entry-header">
				  <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				  <?php financeeducator_edit_link( get_the_ID() ); ?>
			  </header><!-- .entry-header -->
			  <div class="entry-content">
				  <?php
					  the_content();
		  
					  wp_link_pages(
						  array(
							  'before' => '<div class="page-links">' . __( 'Pages:', 'financeeducator' ),
							  'after'  => '</div>',
						  )
					  );
					  ?>
			  </div><!-- .entry-content -->
		  </article><!-- #post-<?php the_ID(); ?> -->
	  </div>
</section>
