<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 * @version 1.0
 */

?>
<!DOCTYPE html>
<html lang="en" <?php language_attributes(); ?> class="no-js no-svg">

<head>
  <meta charset="utf-8">
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <?php wp_head(); ?>
  <!-- Favicons -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon.png" rel="icon">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?php echo get_template_directory_uri(); ?>/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css" rel="stylesheet">
  <!--homepage css-->
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/homepage/index.css">
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/homepage/index2.css">
  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.min.css"> -->
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-bootstrap-4/bootstrap-4.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
  
</head>
<body <?php body_class(); ?> style="padding-top:0px !important;padding-bottom:0px !important;">
<?php wp_body_open(); ?>
  <!-- ======= Top Bar ======= -->
  <section id="topbar" class="d-flex align-items-center fe-bg-dblue">
    <div class="container d-flex justify-content-center justify-content-md-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope d-flex align-items-center fe-color-creamwhite"><a href="mailto:secretariat@fin-ed.org">secretariat@fin-ed.org</a></i>
        <i class="bi bi-phone d-flex align-items-center ms-4 fe-color-creamwhite"><span>+63 933 866 9979</span></i>
      </div>

      <div class="cta d-none d-md-flex align-items-center fe-bg-lightblue">
        <a href="/login" class="scrollto"><i class="bi bi-arrow-up-right-circle-fill"></i>&nbsp; Login</a>
      </div>
    </div>
  </section>

  <!-- ======= Header ======= -->
  <header id="header" class="d-flex align-items-center site-header">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
	      <?php get_template_part( 'template-parts/header/header', 'image' ); ?>
      </div>

      <?php if ( has_nav_menu( 'top' ) ) : ?>
          <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
      <?php endif; ?>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="/">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Advocacies</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li><a href="/blogs">Blogs</a></li>
          <li><a class="nav-link scrollto" href="/contact-us">Contact</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->