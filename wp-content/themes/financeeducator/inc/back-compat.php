<?php
/**
 * Finance Educator back compat functionality
 *
 * Prevents Finance Educator from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage Finance_Educator
 * @since Finance Educator 1.0
 */

/**
 * Prevent switching to Finance Educator on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Finance Educator 1.0
 */
function financeeducator_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'financeeducator_upgrade_notice' );
}
add_action( 'after_switch_theme', 'financeeducator_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Finance Educator on WordPress versions prior to 4.7.
 *
 * @since Finance Educator 1.0
 *
 * @global string $wp_version WordPress version.
 */
function financeeducator_upgrade_notice() {
	printf(
		'<div class="error"><p>%s</p></div>',
		sprintf(
			/* translators: %s: The current WordPress version. */
			__( 'Finance Educator requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'financeeducator' ),
			$GLOBALS['wp_version']
		)
	);
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Finance Educator 1.0
 *
 * @global string $wp_version WordPress version.
 */
function financeeducator_customize() {
	wp_die(
		sprintf(
			/* translators: %s: The current WordPress version. */
			__( 'Finance Educator requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'financeeducator' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'financeeducator_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Finance Educator 1.0
 *
 * @global string $wp_version WordPress version.
 */
function financeeducator_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die(
			sprintf(
				/* translators: %s: The current WordPress version. */
				__( 'Finance Educator requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'financeeducator' ),
				$GLOBALS['wp_version']
			)
		);
	}
}
add_action( 'template_redirect', 'financeeducator_preview' );
